#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Chess.h"
#include "move.h"
#include "AI.h"

int move(int a, int b, int x, int y)
{

    int valid = 1;
    char *tknPiece;

    if (turn != Board[a][b][0])
    {
        return 0;
    }


    switch (Board[a][b][1])
    {
    case 'P':
        valid = checkPawn(a, b, x, y);
        break;
    case 'R':
        valid = checkRook(a, b, x, y);
        break;
    case 'H':
        valid = checkKnight(a, b, x, y);
        break;
    case 'B':
        valid = checkBishop(a, b, x, y);
        break;
    case 'Q':
        valid = checkQueen(a, b, x, y);
        break;
    case 'K':
        valid = checkKing(a, b, x, y);
        break;
    case 'e':
        valid = 0;
        break;
    }

    if (valid == 1)
    {
        Board[x][y] = Board[a][b];
        Board[a][b] = "ee";
    }

    return valid;
}

int checkCommon(int a, int b, int x, int y)
{

    if ( x > 7 || x < 0 || y > 7 || y < 0  )
    {
        return 0;
    }
    else if (x == a && b == y) {
        return 0;
    }
    else if (Board[a][b][0] == Board [x][y][0])
    {
        return 0;
    }

    return 1;
}

int checkRook(int a, int b, int x, int y) {

    if (checkCommon(a, b, x, y) == 0)
    {
        return 0;
    }
    else if (a != x && b != y)
    {
        return 0;
    }
    else if (a == x)
    {
        if (abs(y - b) != 1)
        {
            int dir = (y - b) / (abs(y - b));
            for (int j = b + dir; j != b + dir * abs(y - b); j += dir)
            {
                if (Board[x][j][0] != 'e')
                {
                    return 0;
                }
            }
        }
    }

    else if (b == y)
    {
        if (abs(x - a) != 1)
        {
            int dir = (x - a) / (abs(x - a));
            for (int j = a + dir; j != a + dir * abs(x - a); j += dir)
            {
                if (Board[j][y][0] != 'e')
                {
                    return 0;
                }
            }
        }
    }

    if (testMove(a, b, x, y) == 1)
    {
        return 0;
    }

    return 1;
}

int checkPawn(int a, int b, int x, int y)
{

    if (checkCommon(a, b, x, y) == 0)
    {
        return 0;
    }

    else if (Board[a][b][0] == 'w')
    {
        if (y == b + 1 && (x == a + 1 || x == a - 1))
        {
            if (Board[x][y][0] != 'b')
            {
                return 0;
            }
        }
        else if ( y == b + 1 && x == a)
        {
            if (Board[x][y][0] != 'e')
            {
                return 0;
            }
        }
        else if ( y == b + 2 && x == a)
        {
            if (Board[x][y][0] != 'e' || Board[x][y - 1][0] != 'e' || b != 1)
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }

    else if (Board[a][b][0] == 'b')
    {
        if (y == b - 1 && (x == a + 1 || x == a - 1))
        {
            if (Board[x][y][0] != 'w')
            {
                return 0;
            }
        }
        else if ( y == b - 1 && x == a)
        {
            if (Board[x][y][0] != 'e')
            {
                return 0;
            }
        }
        else if ( y == b - 2 && x == a)
        {
            if (Board[x][y][0] != 'e' || Board[x][y + 1][0] != 'e' || b != 6)
            {
                return 0;
            }
        }
        else {
            return 0;
        }

    }

    if (testMove(a, b, x, y) == 1)
    {
        return 0;
    }

    return 1;

}

int checkKnight(int a, int b, int x, int y)
{

    if (checkCommon(a, b, x, y) == 0)
    {
        return 0;
    }

    if (testMove(a, b, x, y) == 1)
    {
        return 0;
    }

    if ((x == a + 1 || x == a - 1) && (y == b + 2 || y == b - 2))
    {
        return 1;
    }

    if ((x == a + 2 || x == a - 2) && (y == b + 1 || y == b - 1))
    {
        return 1;
    }

    return 0;
}

int checkBishop(int a, int b, int x, int y)
{

    if (checkCommon(a, b, x, y) == 0)
    {
        return 0;
    }
    else if (abs(a - x) != (abs(b - y)) )
    {
        return 0;
    }
    else
    {
        int xdir = (x - a) / (abs(x - a));
        int ydir = (y - b) / (abs(y - b));
        for (int i = 1 * xdir; i != ( x - a ); i += xdir)
        {
            if (Board[a + i][b + i * ydir / xdir ][0] != 'e')
            {
                return 0;
                break;
            }
        }
    }

    if (testMove(a, b, x, y) == 1)
    {
        return 0;
    }

    return 1;
}

int checkQueen(int a, int b, int x, int y)
{

    if (checkBishop(a, b, x, y) == 0 && checkRook(a, b, x, y) == 0)
    {
        return 0;
    }

    if (testMove(a, b, x, y) == 1)
    {
        return 0;
    }

    return 1;
}

int checkKing(int a, int b, int x, int y)
{

    if (checkCommon(a, b, x, y) == 0)
    {
        return 0;
    }
    if (abs(a - x) > 1 || abs(b - y) > 1)
    {
        return 0;
    }

    if (testMove(a, b, x, y) == 1)
    {
        return 0;
    }

    return 1;
}

int testMove(int a, int b, int x, int y)
{

    int check;
    char *tknPiece;
    tknPiece = Board[x][y];

    Board[x][y] = Board[a][b];
    Board[a][b] = "ee";

    check = isCheck(Board[x][y][0]);

    Board[a][b] = Board[x][y];
    Board[x][y] = tknPiece;

    return check;
}

int testPawn(int x, int y)
{

    int dir = 1;

    if (Board[x][y][0] == 'b')
    {
        dir = -1;
    }

    if (checkPawn(x, y, x - 1, y + dir) == 1)
    {
        return 1;
    }
    else if (checkPawn(x, y, x, y + dir) == 1)
    {
        return 1;
    }
    else if (checkPawn(x, y, x + 1, y + dir) == 1)
    {
        return 1;
    }
    else if (checkPawn(x, y, x, y + 2 * dir) == 1)
    {
        return 1;
    }

    return 0;
}

int testRook(int x, int y)
{

    for (int i = 0; i < 8; ++i)
    {
        if (checkRook(x, y, i, y) == 1 || checkRook(x, y, x, i) == 1)
        {
            return 1;
            break;
        }
    }

    return 0;
}

int testBishop (int x, int y)
{

    for (int i = 0; i < 8; ++i)
    {
        if (checkBishop(x, y, i, y - x + i) == 1 || checkBishop(x, y, i, y + x - i))
        {
            return 1;
            break;
        }
    }

    return 0;

}

int testKnight (int x, int y)
{

    if (checkKnight(x, y, x + 1, y + 2) == 1)
    {
        return 1;
    }
    else if (checkKnight(x, y, x + 1, y - 2) == 1)
    {
        return 1;
    }
    else if (checkKnight(x, y, x - 1, y + 2) == 1)
    {
        return 1;
    }
    else if (checkKnight(x, y, x - 1, y - 2) == 1)
    {
        return 1;
    }
    else if (checkKnight(x, y, x + 2, y + 1) == 1)
    {
        return 1;
    }
    else if (checkKnight(x, y, x + 2, y - 1) == 1)
    {
        return 1;
    }
    else if (checkKnight(x, y, x - 2, y + 1) == 1)
    {
        return 1;
    }
    else if (checkKnight(x, y, x - 2, y - 1) == 1)
    {
        return 1;
    }

    return 0;
}

int testQueen (int x, int y)
{

    for (int i = 0; i < 8; ++i)
    {
        if (checkQueen(x, y, i, y) == 1 || checkQueen(x, y, x, i) == 1)
        {
            return 1;
            break;
        }
        if (checkQueen(x, y, i, y - x + i) == 1 || checkQueen(x, y, i, y + x - i))
        {
            return 1;
            break;
        }
    }

    return 0;
}

int testKing (int x, int y )
{

    for (int i = -1; i < 2; ++i)
    {
        for (int j = -1; j < 2; ++j)
        {
            if (checkKing(x, y, x + i, y + j) == 1)
            {
                return 1;
                break;
            }
        }
    }

    return 0;
}