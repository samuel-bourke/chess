#ifndef CHESS_H
#define CHESS_H

char* Board[8][8];
char turn;
static int maxLevel = 2;

void printBoard();
void setupBoard();
void swapMove();
int isCheck(char color);
int isCheckMate(char color);
int boardValue();
int stalemate();
void printMove(int a, int b, int x, int y);

#endif