#include <stdio.h>
#include <stdlib.h>

#ifndef MOVECHECK_H_INCLUDED
#define MOVECHECK_H_INCLUDED

int move(int a, int b, int x, int y);

int checkCommon(int a, int b, int x, int y);
int checkRook(int a, int b, int x, int y);
int checkPawn(int a, int b, int x, int y);
int checkKnight(int a, int b, int x, int y);
int checkBishop(int a, int b, int x, int y);
int checkQueen(int a, int b, int x, int y);
int checkKing(int a, int b, int x, int y);

int testMove(int x, int y, int a, int b);
int testPawn(int x, int y);
int testRook(int x, int y);
int testBishop(int x, int y);
int testKnight(int x, int y);
int testQueen(int x, int y);
int testKing(int x, int y);

#endif