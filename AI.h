#ifndef AI_H
#define AI_H

int numMoves;

void AIMove(int *a, int *b, int *x, int *y);
int compMove(int levels);
int moveValue(int a, int b, int x, int y);
void addMove(int a, int b, int x, int y, int moves[][5]);
void sortMoves(int moves[][5], int totalMoves);

void compPawn(int x, int y, int moves[][5]);
void compRook(int x, int y, int moves[][5]);
void compBishop(int x, int y, int moves[][5]);
void compKnight(int x, int y, int moves[][5]);
void compQueen(int x, int y, int moves[][5]);
void compKing(int x, int y, int moves[][5]);

#endif