#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Chess.h"
#include "move.h"
#include "AI.h"

void AIMove(int *a, int *b, int *x, int *y)
{

    int moveScore;

    int breakdown[4];

    moveScore = compMove(maxLevel);

    for (int i = 0; i < 4; ++i)
    {
        breakdown[i] = moveScore % 10;
        moveScore /= 10;
    }

    *a = breakdown[3];
    *b = breakdown[2];
    *x = breakdown[1];
    *y = breakdown[0];

}

int compMove(int levels)
{

    int moves [140][5];
    int numBest = 1;
    int moveNum;
    int total =  0;
    numMoves = 0;
    int totalMoves = 0;

    for (int i = 0; i < 8; ++i)
    {
        for (int j = 0; j < 8; ++j)
        {
            if (Board[i][j][0] == turn)
            {
                switch (Board[i][j][1])
                {
                    case 'P':
                        compPawn(i, j, moves);
                        break;
                    case 'R':
                        compRook(i, j, moves);
                        break;
                    case 'H':
                        compKnight(i, j, moves);
                        break;
                    case 'B':
                        compBishop(i, j, moves);
                        break;
                    case 'Q':
                        compQueen(i, j, moves);
                        break;
                    case 'K':
                        compKing(i, j, moves);
                        break;
                }
            }
        }
    }

    totalMoves = numMoves;
    numMoves = 0;

    if (levels != 0 )
    {
        for (int i = 0; i < totalMoves; ++i)
        {
            char *tknPiece;

            tknPiece = Board[moves[i][2]][moves[i][3]];

            Board[moves[i][2]][moves[i][3]] = Board[moves[i][0]][moves[i][1]];
            Board[moves[i][0]][moves[i][1]] = "ee";

            swapMove();
            moves[i][4] += compMove(levels - 1);
            swapMove();

            Board[moves[i][0]][moves[i][1]] = Board[moves[i][2]][moves[i][3]];
            Board[moves[i][2]][moves[i][3]] = tknPiece;
        }

    }

    if (levels == maxLevel)
    {

        sortMoves(moves, totalMoves);

        for (int i = 1; i < totalMoves; ++i)
        {
            if (moves[i][4] != moves[0][4])
            {
                break;
            }
            numBest += 1;

        }

        moveNum = rand() % numBest;
        return 1000 * moves[moveNum][0] + 100 * moves[moveNum][1] + 10 * moves[moveNum][2] + moves[moveNum][3];
    }

    for (int i = 0; i < totalMoves; ++i)
    {
        total += moves[i][4];
    }
    if (total != 0)
    {
        return (total / totalMoves);
    }
    return 0;

}


void addMove(int a, int b, int x, int y, int moves[][5])
{
    moves[numMoves][0] = a;
    moves[numMoves][1] = b;
    moves[numMoves][2] = x;
    moves[numMoves][3] = y;
    moves[numMoves][4] = moveValue(a, b, x, y);
    numMoves += 1;
}

void compPawn(int x, int y, int moves[][5])
{
    int dir = 1;

    if (Board[x][y][0] == 'b')
    {
        dir = -1;
    }
    if (checkPawn(x, y, x - 1, y + dir) == 1)
    {
        addMove(x, y, x - 1, y + dir, moves);

    }
    if (checkPawn(x, y, x, y + dir) == 1)
    {
        addMove(x, y, x, y + dir, moves);

    }
    if (checkPawn(x, y, x + 1, y + dir) == 1)
    {
        addMove(x, y, x + 1, y + dir, moves);

    }
    if (checkPawn(x, y, x, y + 2 * dir) == 1)
    {
        addMove(x, y, x, y + 2 * dir, moves);
    }
}

void compRook(int x, int y, int moves[][5])
{

    for (int i = 0; i < 8; ++i)
    {
        if (checkRook(x, y, i, y) == 1)
        {
            addMove(x, y, i, y, moves);
        }
        if (checkRook(x, y, x, i) == 1)
        {
            addMove(x, y, x, i, moves);
        }
    }
}

void compBishop (int x, int y, int moves[][5])
{

    for (int i = 0; i < 8; ++i)
    {
        if (checkBishop(x, y, i, y - x + i) == 1)
        {
            addMove(x, y, i, y - x + i, moves);
        }

        if (checkBishop(x, y, i, y + x - i) == 1)
        {
            addMove(x, y, i, y + x - i, moves);
        }
    }
}

void compKnight (int x, int y, int moves[][5])
{
    if (checkKnight(x, y, x + 1, y + 2) == 1)
    {
        addMove(x, y, x + 1, y + 2, moves);
    }
    if (checkKnight(x, y, x + 1, y - 2) == 1)
    {
        addMove(x, y, x + 1, y - 2, moves);
    }
    if (checkKnight(x, y, x - 1, y + 2) == 1)
    {
        addMove(x, y, x - 1, y + 2, moves);
    }
    if (checkKnight(x, y, x - 1, y - 2) == 1)
    {
        addMove(x, y, x - 1, y - 2, moves);
    }
    if (checkKnight(x, y, x + 2, y + 1) == 1)
    {
        addMove(x, y, x + 2, y + 1, moves);
    }
    if (checkKnight(x, y, x + 2, y - 1) == 1)
    {
        addMove(x, y, x + 2, y - 1, moves);
    }
    if (checkKnight(x, y, x - 2, y + 1) == 1)
    {
        addMove(x, y, x - 2, y + 1, moves);
    }
    if (checkKnight(x, y, x - 2, y - 1) == 1)
    {
        addMove(x, y, x - 2, y - 1, moves);
    }
}


void compQueen (int x, int y, int moves[][5])
{
    for (int i = 0; i < 8; ++i)
    {
        if (checkRook(x, y, i, y) == 1)
        {
            addMove(x, y, i, y, moves);
        }
        if (checkRook(x, y, x, i) == 1)
        {
            addMove(x, y, x, i, moves);
        }
        if (checkBishop(x, y, i, y - x + i) == 1)
        {
            addMove(x, y, i, y - x + i, moves);
        }
        if (checkBishop(x, y, i, y + x - i) == 1)
        {
            addMove(x, y, i, y + x - i, moves);
        }
    }
}

void compKing(int x, int y, int moves[][5])
{

    for (int i = -1; i < 2; ++i)
    {
        for (int j = -1; j < 2; ++j)
        {
            if (checkKing(x, y, x + i, y + j) == 1)
            {
                addMove(x, y, x + i, y + j, moves);
            }
        }
    }
}


int moveValue(int a, int b, int x, int y)
{

    int value;

    char *tknPiece;


    tknPiece = Board[x][y];

    Board[x][y] = Board[a][b];
    Board[a][b] = "ee";

    value = boardValue();

    Board[a][b] = Board[x][y];
    Board[x][y] = tknPiece;

    return value;

}

void sortMoves(int moves[][5], int totalMoves)
{


    int smallest[2];
    int hold;

    for (int i = 0; i < totalMoves; ++i)
    {

        for (int j = 0; j < totalMoves - 1; ++j)
        {
            if (((moves[j][4] > moves[j + 1][4]) && (turn == 'b')) || ((moves[j][4] < moves[j + 1][4]) && (turn == 'w')))
            {
                for (int k = 0; k < 5; ++k)
                {
                    hold = moves[j][k];
                    moves[j][k] = moves[j + 1][k];
                    moves[j + 1][k] = hold;
                }
            }
        }
    }
}