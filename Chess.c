#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "Chess.h"
#include "move.h"
#include "AI.h"

int main()
{

    setupBoard();
    printBoard();

    int levels = 3;
    int n = 0;

    srand(time(NULL));

    turn = 'w';

    int a, b, x, y;
    while (1 == 1)
    {
        n = n + 1;

        do
        {
            if (turn == 'w')
            {
                //scanf("%i %i %i %i", &a, &b, &x, &y);
                AIMove(&a, &b, &x, &y);
            }
            else
            {
                AIMove(&a, &b, &x, &y);
            }

        } while (move(a, b, x, y) == 0);

        printBoard();

        printMove(a, b, x, y);

        if (isCheckMate('w') == 1)
        {
            printBoard();
            printf("Black Wins!\n");
            break;
        }
        else if (isCheckMate('b') == 1)
        {
            printBoard();
            printf("White Wins!\n");
            break;
        }
        else if (stalemate() == 1)
        {
            printBoard();
            printf("Stalemate!\n");
            break;
        }
        if ( n > 1000) {
            printf("Stalemate!\n");
            break;
        }
        swapMove();
        //sleep(1);
    }
    printf("%i\n", n);
    return 0;

}

void printBoard()
{

    system("clear");
    printf("    0   1   2   3   4   5   6   7  \n");
    printf("  =================================\n");

    for (int j = 7; j >= 0; --j)
    {
        printf("%i |", j);
        for (int i = 0; i < 8; ++i)
        {
            if (Board[i][j][0] == 'e')
            {
                printf("   |");
            }
            else if (Board[i][j][0] == 'b')
            {
                printf("\x1B[31m %c\x1B[0m |", Board[i][j][1]);
            }
            else if (Board[i][j][0] == 'w')
            {
                printf("\x1B[36m %c\x1B[0m |", Board[i][j][1]);
            }

        }
        printf(" %i", j);
        printf("\n  =================================\n");

    }
    printf("    0   1   2   3   4   5   6   7  \n\n");

}

void swapMove()
{

    if (turn == 'w')
    {
        turn = 'b';
    }
    else
    {
        turn = 'w';
    }
}

void setupBoard()
{

    for (int i = 0; i < 8; ++i)
    {
        for (int j = 0; j < 8; ++j)
        {
            Board[i][j] = "ee";
        }
    }

    for (int i = 0; i < 8; ++i)
    {
        Board[i][6] = "bP";
        Board[i][1] = "wP";
    }

    Board[0][7] = "bR";
    Board[1][7] = "bH";
    Board[2][7] = "bB";
    Board[3][7] = "bQ";
    Board[4][7] = "bK";
    Board[5][7] = "bB";
    Board[6][7] = "bH";
    Board[7][7] = "bR";

    Board[0][0] = "wR";
    Board[1][0] = "wH";
    Board[2][0] = "wB";
    Board[3][0] = "wQ";
    Board[4][0] = "wK";
    Board[5][0] = "wB";
    Board[6][0] = "wH";
    Board[7][0] = "wR";

}

int isCheck(char color)
{

    int x, y;
    int valid = 0;

    char atkCol = 'w';

    if (color == 'w')
    {
        atkCol = 'b';
    }

    for (int i = 0; i < 8; ++i)
    {
        for (int j = 0; j < 8; ++j)
        {
            if (Board[i][j][0] == color && Board[i][j][1] == 'K')
            {
                x = i;
                y = j;
                break;
            }
        }
    }

    for (int i = 0; i < 8; ++i)
    {
        for (int j = 0; j < 8; ++j)
        {
            if (Board[i][j][0] == atkCol)
            {

                switch (Board[i][j][1])
                {
                case 'P':
                    valid = checkPawn(i, j, x, y);
                    break;
                case 'R':
                    valid = checkRook(i, j, x, y);
                    break;
                case 'H':
                    valid = checkKnight(i, j, x, y);
                    break;
                case 'B':
                    valid = checkBishop(i, j, x, y);
                    break;
                case 'Q':
                    valid = checkQueen(i, j, x, y);
                    break;
                case 'K':
                    valid = checkKing(i, j, x, y);
                    break;
                }
                if (valid == 1)
                {
                    return 1;
                }
            }
        }
    }

    return 0;
}


int boardValue()
{

    int coef = 0;
    int total = 0;

    for (int i = 0; i < 8; ++i)
    {
        for (int j = 0; j < 8; ++j)
        {

            if (Board[i][j][0] == 'w')
            {

                coef = 1;
            }
            else if (Board[i][j][0] == 'b')
            {
                coef = -1;
            }

            if (coef != 0)
            {
                switch (Board[i][j][1])
                {
                case 'P':
                    total += coef * 1;
                    break;
                case 'R':
                    total += coef * 10;
                    break;
                case 'H':
                    total += coef * 6;
                    break;
                case 'B':
                    total += coef * 6;
                    break;
                case 'Q':
                    total += coef * 18;
                    break;
                }
            }
        }
    }

    if (isCheckMate('w'))
    {
        total += -20;
    }
    else if (isCheckMate('b'))
    {
        total += 20;
    }

    return total;
}

int isCheckMate(char color)
{
    int valid = 0;
    for (int i = 0; i < 8; ++i)
    {
        for (int j = 0; j < 8; ++j)
        {
            if (Board[i][j][0] == color)
            {
                switch (Board[i][j][1])
                {
                case 'P':
                    valid = testPawn(i, j);
                    break;
                case 'R':
                    valid = testRook(i, j);
                    break;
                case 'B':
                    valid = testBishop(i, j);
                    break;
                case 'H':
                    valid = testKnight(i, j);
                    break;
                case 'Q':
                    valid = testQueen(i, j);
                    break;
                case 'K':
                    valid = testKing(i, j);
                    break;
                }
                if (valid == 1)
                {
                    return 0;
                }
            }
        }
    }

    return 1;
}

int stalemate()
{
    for (int i = 0; i < 8; ++i)
    {
        for (int j = 0; j < 8; ++j)
        {
            if (Board[i][j][1] != 'K' && Board[i][j][1] != 'e') {
                return 0;
            }
        }
    }
    return 1;
}

void printMove(int a, int b, int x, int y)
{

    if (Board[x][y][0] == 'w')
    {
        printf("White ");
    }
    else
    {
        printf("Black ");
    }
    switch (Board[x][y][1])
    {
    case 'P':
        printf("Pawn from (%i,%i) to (%i,%i)\n", a, b, x, y );
        break;
    case 'R':
        printf("Rook from (%i,%i) to (%i,%i)\n", a, b, x, y );
        break;
    case 'H':
        printf("Knight from (%i,%i) to (%i,%i)\n", a, b, x, y );
        break;
    case 'B':
        printf("Bishop from (%i,%i) to (%i,%i)\n", a, b, x, y );
        break;
    case 'Q':
        printf("Queen from (%i,%i) to (%i,%i)\n", a, b, x, y );
        break;
    case 'K':
        printf("King from (%i,%i) to (%i,%i)\n", a, b, x, y );
        break;
    }
}